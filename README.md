# scripts

my miscellaneous scripts

## dwm_timer
loop that outputs the time to dwm's statusbar

usage: dwm_timer

## poweropt
prompt for power options

usage: poweropt [power command] [input menu command]

## shot
screenshot script using maim and xclip

usage: shot [shot|select]

## upload
upload script using curl uploading to 0x0.st

usage: upload [file]

## view
mpv script to watch video with max video settings of 1440p

usage: view "[link]"

## vol
volume management script using amixer and notify-send

usage: vol [plus|minus|show]
